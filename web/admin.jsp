<%-- 
    Document   : admin
    Created on : Jul 17, 2019, 7:22:46 PM
    Author     : tienh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<c:if test="${sessionScope.username==null}">
    <jsp:forward page="login.jsp"/>
</c:if>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
    <body>
        <h1>Welcome, ${sessionScope.username}</h1>
        <form action="manageServlet" method="POST">
            EmployeeID : <input type="text" name="employeeid" value="${employee.employeeid}"/><br/>
            FirstName : <input type="text" name="firstname" value="${employee.firstname}"/><br/>
            LastName : <input type="text" name="lastname" value="${employee.lastname}"/><br/>
            Email : <input type="text" name="email" value="${employee.email}" /><br/>
            
            <input type="submit" name="action" value="ADD"/> |
            <input type="submit" name="action" value="EDIT"/> |
            <input type="submit" name="action" value="DELETE"/> |
            <input type="submit" name="action" value="VIEW"/>
        </form>
            
            <table>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                </tr>
                <c:forEach var="emp" items="${getAllEmployee}">
                <tr>
                    <td>${emp.employeeid}</td>
                    <td>${emp.firstname}</td>
                    <td>${emp.lastname}</td>
                    <td>${emp.email}</td>
                </tr>
                </c:forEach>
            </table>

    </body>
</html>
